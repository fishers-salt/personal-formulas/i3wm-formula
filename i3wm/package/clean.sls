# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_config_clean = tplroot ~ '.config.clean' %}
{%- from tplroot ~ "/map.jinja" import mapdata as i3wm with context %}

include:
  - {{ sls_config_clean }}

i3wm-package-clean-pkgs-removed:
  pkg.removed:
    - names: {{ i3wm.pkgs }}
    - require:
      - sls: {{ sls_config_clean }}
