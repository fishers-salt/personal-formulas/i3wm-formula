# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as i3wm with context %}

i3wm-package-install-pkgs-installed:
  pkg.installed:
    - pkgs: {{ i3wm.pkgs }}
