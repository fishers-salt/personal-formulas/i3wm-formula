# -*- coding: utf-8 -*-
# vim: ft=i3.jinja
########################################################################
# File managed by Salt at <{{ source }}>.
# Your changes will be overwritten.
########################################################################

# i3 config file (v4)
#
# Please see https://i3wm.org/docs/userguide.html for a complete reference!

set $mod Mod4

# The combination of xss-lock, nm-applet and pactl is a popular choice, so
# they are included here as an example. Modify as you see fit.

# xss-lock grabs a logind suspend inhibit lock and will use i3lock to lock the
# screen before suspend. Use loginctl lock-session to lock your screen.
exec --no-startup-id xss-lock --transfer-sleep-lock -- i3lock --nofork --color=000000 --image=/home/stooj/pictures/lock-screen.png
bindsym Control+$mod+l exec loginctl lock-session

# NetworkManager is the most popular way to manage wireless networks on Linux,
# and nm-applet is a desktop environment-independent system tray GUI for it.
# exec --no-startup-id nm-applet

# Start up picom compositor
exec --no-startup-id picom --daemon

# Enable wallpaper script
exec --no-startup-id systemctl --user start change_i3_background.service
exec --no-startup-id systemctl --user start change_i3_background.timer

# Adding this here because GDM disables CPP when merging.
# https://bbs.archlinux.org/viewtopic.php?id=169112
exec --no-startup-id xrdg -merge {{ home }}/.Xresources

# Launch notification daemon
exec --no-startup-id /usr/bin/dunst

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# Disable mouse focus
# While I find focus-follows-mouse essential with desktop environments, it gets
# in the way when you are selected all focus using the keyboard.
#
# For example, I will ask rofi-pass to type into a terminal, but in the time it
# takes to open rofi-pass and find what I want, focus has switched to wherever
# mymouse is pointing (often the wrong place)
focus_follows_mouse no

# Setting border style to pixel eliminates title bars. The border style normal
# allows you to adjust edge border width while keeping your title bar.
default_border pixel

# start a terminal
bindsym $mod+Return exec i3-sensible-terminal

# kill focused window
bindsym $mod+Shift+q kill

# start rofi (a program launcher)
bindsym $mod+d exec rofi -show run
bindsym $mod+Shift+D exec rofi -show drun
bindsym $mod+c exec rofi -show ssh
bindsym $mod+p exec rofi-pass
bindsym $mod+Shift+S exec rofi -show emoji -modi emoji -config ~/.config/rofi/emoji.rasi

# Control notifications
## Close notification
bindsym Control+space exec dunstctl close
## Close all notification
bindsym Control+Shift+space exec dunstctl close-all
## Show last message
bindsym Control+grave exec dunstctl history-pop

# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+o split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# {,un}pin a floating window
bindsym $mod+Shift+p sticky toggle

# focus the parent container
bindsym $mod+a focus parent

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

# switch to workspace
bindsym $mod+1 workspace number $ws1
bindsym $mod+2 workspace number $ws2
bindsym $mod+3 workspace number $ws3
bindsym $mod+4 workspace number $ws4
bindsym $mod+5 workspace number $ws5
bindsym $mod+6 workspace number $ws6
bindsym $mod+7 workspace number $ws7
bindsym $mod+8 workspace number $ws8
bindsym $mod+9 workspace number $ws9
bindsym $mod+0 workspace number $ws10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace number $ws1
bindsym $mod+Shift+2 move container to workspace number $ws2
bindsym $mod+Shift+3 move container to workspace number $ws3
bindsym $mod+Shift+4 move container to workspace number $ws4
bindsym $mod+Shift+5 move container to workspace number $ws5
bindsym $mod+Shift+6 move container to workspace number $ws6
bindsym $mod+Shift+7 move container to workspace number $ws7
bindsym $mod+Shift+8 move container to workspace number $ws8
bindsym $mod+Shift+9 move container to workspace number $ws9
bindsym $mod+Shift+0 move container to workspace number $ws10

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+E exec ~/.config/i3/scripts/exit_menu

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym h resize shrink width 10 px or 10 ppt
        bindsym j resize grow height 10 px or 10 ppt
        bindsym k resize shrink height 10 px or 10 ppt
        bindsym l resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape or $mod+r
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $mod+r mode "default"
}

bindsym $mod+r mode "resize"

################
#  Media keys  #
################

set $refresh_i3status killall -SIGUSR1 i3status
{%- for action, data in i3wm.get('media-key-bindings', {}).items() %}
# {{ action }}
bindsym {{ data.keybind }} exec {% if data.flags is defined %}{{ data.flags }} {% endif %}{{ data.command }}
{%- endfor %}

###########
#  THEME  #
###########


# set primary gruvbox colorscheme colors
set $bg #282828
set $red #cc241d
set $green #98971a
set $yellow #d79921
set $blue #458588
set $purple #b16286
set $aqua #689d68
set $gray #a89984
set $darkgray #1d2021
set $white #ffffff

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango:fira code medium 10

# This font is widely installed, provides lots of unicode glyphs, right-to-left
# text rendering and scalability on retina/hidpi displays (thanks to pango).
#font pango:DejaVu Sans Mono 8

#start of window title bars & borders section
# green gruvbox
# class                 border|backgr|text|indicator|child_border
#client.focused          $green $green $darkgray $purple $darkgray
#client.focused_inactive $darkgray $darkgray $yellow $purple $darkgray
#client.unfocused        $darkgray $darkgray $yellow $purple $darkgray
#client.urgent           $red $red $white $red $red

# blue gruvbox
# class                 border|backgr|text|indicator|child_border
client.focused          $blue $blue $darkgray $purple $darkgray
client.focused_inactive $darkgray $darkgray $yellow $purple $darkgray
client.unfocused        $darkgray $darkgray $yellow $purple $darkgray
client.urgent           $red $red $white $red $red
#end of window title bars & borders section

# Run startup scripts
exec --no-startup-id ~/.config/i3/autostart
