# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as i3wm with context %}

{% if salt['pillar.get']('i3wm-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_i3wm', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

i3wm-config-file-profile-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.xprofile

i3wm-config-file-exit-script-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/i3/scripts/exit_menu

i3wm-config-file-autostart-script-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/i3/autostart

i3wm-config-file-config-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/i3/config

i3wm-config-file-config-scripts-dir-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/i3/scripts

i3wm-config-file-config-autostart-dir-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/i3/autostart.d

i3wm-config-file-config-config-d-dir-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/i3/config.d

i3wm-config-file-config-dir-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/i3

{% endif %}
{% endfor %}
{% endif %}
