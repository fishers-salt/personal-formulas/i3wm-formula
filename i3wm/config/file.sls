# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as i3wm with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

{% if salt['pillar.get']('i3wm-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_i3wm', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

i3wm-config-file-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

i3wm-config-file-config-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/i3
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - i3wm-config-file-user-{{ name }}-present

i3wm-config-file-config-config-d-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/i3/config.d
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - i3wm-config-file-config-dir-{{ name }}-managed

i3wm-config-file-config-autostart-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/i3/autostart.d
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - i3wm-config-file-config-dir-{{ name }}-managed

i3wm-config-file-config-scripts-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/i3/scripts
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - i3wm-config-file-config-dir-{{ name }}-managed

i3wm-config-file-config-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/i3/config
    - source: {{ files_switch([
                   name ~ '-i3-config.tmpl', 'i3-config.tmpl'],
                 lookup='i3wm-config-file-config-' ~ name ~ '-managed'
                 )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - context:
        home: {{ home }}
        i3wm: {{ i3wm }}
    - require:
      - i3wm-config-file-config-dir-{{ name }}-managed

i3wm-config-file-autostart-script-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/i3/autostart
    - source: {{ files_switch([
                   name ~ '-i3-autostart.sh.tmpl', 'i3-autostart.sh.tmpl'],
                 lookup='i3wm-config-file-autostart-script-' ~ name ~ '-managed'
                 )
              }}
    - mode: '0755'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - i3wm-config-file-config-dir-{{ name }}-managed

i3wm-config-file-exit-script-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/i3/scripts/exit_menu
    - source: {{ files_switch([
                   name ~ 'exit_menu.tmpl', 'exit_menu.tmpl'],
                 lookup='i3wm-config-file-exit-script-' ~ name ~ '-managed'
                 )
              }}
    - mode: '0755'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - i3wm-config-file-config-scripts-dir-{{ name }}-managed

{%- set terminal = user.get('preferred_terminal', 'terminator') %}
i3wm-config-file-profile-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.xprofile
    - source: {{ files_switch([
                   name ~ 'xprofile.tmpl', 'xprofile.tmpl'],
                 lookup='i3wm-config-file-profile' ~ name ~ '-managed'
                 )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - context:
        terminal: {{ terminal }}
    - require:
      - i3wm-config-file-user-{{ name }}-present
{% endif %}

{%- set i3_user_config = user.get('i3wm', {}) %}
{%- set symlink_target = i3_user_config.get('wallpaper_location', none) %}
{%- if symlink_target is not none %}
i3wm-config-file-systemd-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/systemd
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - require:
      - i3wm-config-file-config-dir-{{ name }}-managed

i3wm-config-file-systemd-user-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/systemd/user
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - require:
      - i3wm-config-file-systemd-dir-{{ name }}-managed

i3wm-config-file-wallpaper-service-unit-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/systemd/user/change_i3_background.service
    - source: {{ files_switch([
                   name ~ 'change_i3_background.service.tmpl',
                   'change_i3_background.service.tmpl'
                   ],
                 lookup='i3wm-config-file-wallpaper-service-unit-' ~ name ~ '-managed'
                 )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - context:
        home: {{ home }}
    - require:
      - i3wm-config-file-systemd-user-dir-{{ name }}-managed

i3wm-config-file-wallpaper-timer-unit-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/systemd/user/change_i3_background.timer
    - source: {{ files_switch([
                   name ~ 'change_i3_background.timer.tmpl',
                   'change_i3_background.timer.tmpl'
                   ],
                 lookup='i3wm-config-file-wallpaper-service-unit-' ~ name ~ '-managed'
                 )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - i3wm-config-file-systemd-user-dir-{{ name }}-managed

i3wm-config-file-bin-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/bin
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - i3wm-config-file-user-{{ name }}-present

i3wm-config-file-wallpaper-bin-script-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/bin/change_wallpaper
    - source: {{ files_switch([
                   name ~ '-change_wallpaper.py.tmpl',
                   'change_wallpaper.py.tmpl'
                   ],
                 lookup='i3wm-config-file-wallpaper-bin-script-' ~ name ~ '-managed'
                 )
              }}
    - mode: '0755'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - i3wm-config-file-bin-{{ name }}-managed

i3wm-config-file-wallpaper-symlink-{{ name }}-managed:
  file.symlink:
    - name: {{ home }}/.config/desktop-backgrounds
    - target: {{ symlink_target }}
    - user: {{ name }}
    - group: {{ user_group }}
    - require:
      - i3wm-config-file-config-dir-{{ name }}-managed

i3wm-config-file-wallpaper-autostart-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/i3/autostart.d/wallpaper
    - source: {{ files_switch([
                   name ~ '-wallpaper-autostart.tmpl',
                   'wallpaper-autostart.tmpl'
                   ],
                 lookup='i3wm-config-file-wallpaper-autostart-' ~ name ~ '-managed'
                 )
              }}
    - mode: '0755'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - i3wm-config-file-config-autostart-dir-{{ name }}-managed
{%- endif %}
{% endfor %}
{% endif %}
