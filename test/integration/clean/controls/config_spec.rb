# frozen_string_literal: true

control 'i3wm-config-file-profile-auser-managed' do
  title 'should not exist'

  describe file('/home/auser/.xprofile') do
    it { should_not exist }
  end
end

control 'i3wm-config-clean-config-dir-auser-absent' do
  title 'should not exist'

  describe directory('/home/auser/.config/i3') do
    it { should_not exist }
  end
end

control 'i3wm-config-clean-config-config-d-dir-auser-absent' do
  title 'should not exist'

  describe directory('/home/auser/.config/i3/config.d') do
    it { should_not exist }
  end
end

control 'i3wm-config-clean-config-autostart-dir-auser-absent' do
  title 'should not exist'

  describe directory('/home/auser/.config/i3/autostart.d') do
    it { should_not exist }
  end
end

control 'i3wm-config-clean-config-scripts-dir-auser-absent' do
  title 'should not exist'

  describe directory('/home/auser/.config/i3/scripts') do
    it { should_not exist }
  end
end

control 'i3wm-config-clean-config-auser-absent' do
  title 'should match desired lines'

  describe file('/home/auser/.config/i3/config') do
    it { should_not exist }
  end
end

control 'i3wm-config-clean-autostart-script-auser-absent' do
  title 'should match desired lines'

  describe file('/home/auser/.config/i3/autostart') do
    it { should_not exist }
  end
end

control 'i3wm-config-clean-exit-script-auser-absent' do
  title 'should match desired lines'

  describe file('/home/auser/.config/i3/scripts/exit_menu') do
    it { should_not exist }
  end
end
