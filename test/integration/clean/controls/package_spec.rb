# frozen_string_literal: true

packages = %w[i3-wm i3lock i3status xss-lock]

packages.each do |pkg|
  control "i3wm-package-clean-pkg-#{pkg}-removed" do
    title "#{pkg} should not be installed"

    describe package(pkg) do
      it { should_not be_installed }
    end
  end
end
