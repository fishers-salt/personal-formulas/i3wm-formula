# frozen_string_literal: true

control 'i3wm-config-file-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'i3wm-config-file-config-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/i3') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'i3wm-config-file-config-config-d-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/i3/config.d') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'i3wm-config-file-config-autostart-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/i3/autostart.d') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'i3wm-config-file-config-scripts-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/i3/scripts') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'i3wm-config-file-config-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/i3/config') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('# i3 config file (v4)') }
    its('content') do
      should include('bindsym XF86MonBrightnessDown exec xbacklight -dec 10')
    end
    its('content') { should include('exec --no-startup-id ~/.config/i3/autostart') }
  end
end

control 'i3wm-config-file-autostart-script-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/i3/autostart') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('#!/bin/sh') }
    its('content') do
      should include('for file in "$HOME"/.config/i3/autostart.d/*; do')
    end
  end
end

control 'i3wm-config-file-exit-script-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/i3/scripts/exit_menu') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('[ "$select" = "Cancel" ] && exit 0') }
  end
end

control 'i3wm-config-file-profile-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.xprofile') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('#  Your changes will be overwritten.') }
    its('content') { should include('TERMINAL=/usr/bin/urxvt') }
  end
end

control 'i3wm-config-file-systemd-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/systemd') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'i3wm-config-file-systemd-user-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/systemd/user') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'i3wm-config-file-wallpaper-service-unit-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/systemd/user/change_i3_background.service') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('ExecStart=/home/auser/bin/change_wallpaper') }
  end
end

control 'i3wm-config-file-wallpaper-timer-unit-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/systemd/user/change_i3_background.timer') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('OnCalendar=minutely') }
  end
end

control 'i3wm-config-file-bin-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/bin') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'i3wm-config-file-wallpaper-bin-script-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/bin/change_wallpaper') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('STRETCH_ACROSS_DUAL_MONITOR = False') }
  end
end

control 'i3wm-config-file-wallpaper-symlink-auser-managed' do
  title 'it should be configured correctly'

  describe file('/home/auser/.config/desktop-backgrounds') do
    it { should be_symlink }
  end
end

control 'i3wm-config-file-wallpaper-autostart-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/i3/autostart.d/wallpaper') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
    its('content') { should include('#  Your changes will be overwritten.') }
    its('content') do
      should include('systemctl start --user change_i3_background.timer')
    end
  end
end
