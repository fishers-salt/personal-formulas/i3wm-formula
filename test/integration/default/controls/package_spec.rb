# frozen_string_literal: true

packages = %w[i3-wm i3lock i3status xss-lock]

packages.each do |pkg|
  control "i3wm-package-install-pkg-#{pkg}-installed" do
    title "#{pkg} should be installed"

    describe package(pkg) do
      it { should be_installed }
    end
  end
end
